package pt.tiagoedgar.poc.caffeine;

import pt.tiagoedgar.poc.caffeine.application.PersonService;
import pt.tiagoedgar.poc.caffeine.infrastructure.cache.PersonCache;
import pt.tiagoedgar.poc.caffeine.infrastructure.repository.PersonRepository;
import pt.tiagoedgar.poc.caffeine.model.Person;
import java.util.Arrays;
import java.util.List;

/**
 * https://www.baeldung.com/java-caching-caffeine
 */
public class CaffeinePoc {

    /** CACHE SETUP **/
    private static final int CACHE_MAX_SIZE = 10;
    private static final boolean CACHE_INITIAL_POPULATE = true;
    /*****************/

    public static void main(String[] args) {
        System.out.println("Java Caffeine cache POC!");

        List<Person> persons = Arrays.asList(
                new Person("Tiago", "tiago@email.com", 36),
                new Person("Bruno", "bruno@email.com", 30),
                new Person("Eric", "bruno@email.com", 40)
        );

        PersonService personService = new PersonCache(new PersonRepository(persons), CACHE_MAX_SIZE, CACHE_INITIAL_POPULATE);
        personService.readByName("Tiago");
        personService.readByName("Tiago");
        personService.readByName("João");
        personService.readByName("Tiago");
        personService.readByName("Bruno");
        personService.readByName("Bruno");
        personService.readByName("Eric");
        personService.readByName("Eric");
        personService.readByName("Tiago");
        personService.readByName("Eric");
        personService.readByName("Bruno");
    }

}
