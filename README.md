# Caffeine POC

This projects is just a POC to understand how caffeine works. 

Caffeine is an in memory high-performance caching library for Java.

**Note:** This project does not have a Database as it should be in a Production environment. The repository is also in memory, but it represents the access to the database. The goal is to check that the cache does not call the repository if the Person Object is in cache.

## Test it
To test it, just setup different values in CaffeinePoc.java and run the project

### Setup
```java
/** CACHE SETUP **/
private static final int CACHE_MAX_SIZE = 10;
private static final boolean CACHE_INITIAL_POPULATE = true;
/*****************/
```

### Input
- Person{name='Tiago', email='tiago@email.com', age=36}
- Person{name='Bruno', email='bruno@email.com', age=30}
- Person{name='Eric', email='bruno@email.com', age=40}

### Output
```bash
Java Caffeine cache POC!
------------------ Tiago -----------------
Cache Person (Tiago): Person{name='Tiago', email='tiago@email.com', age=36}
------------------ Tiago -----------------
Cache Person (Tiago): Person{name='Tiago', email='tiago@email.com', age=36}
------------------ João -----------------
-----> Repository Person (João): null
Cache Person (João): null
------------------ Tiago -----------------
Cache Person (Tiago): Person{name='Tiago', email='tiago@email.com', age=36}
------------------ Bruno -----------------
Cache Person (Bruno): Person{name='Bruno', email='bruno@email.com', age=30}
------------------ Bruno -----------------
Cache Person (Bruno): Person{name='Bruno', email='bruno@email.com', age=30}
------------------ Eric -----------------
Cache Person (Eric): Person{name='Eric', email='bruno@email.com', age=40}
------------------ Eric -----------------
Cache Person (Eric): Person{name='Eric', email='bruno@email.com', age=40}
------------------ Tiago -----------------
Cache Person (Tiago): Person{name='Tiago', email='tiago@email.com', age=36}
------------------ Eric -----------------
Cache Person (Eric): Person{name='Eric', email='bruno@email.com', age=40}
------------------ Bruno -----------------
Cache Person (Bruno): Person{name='Bruno', email='bruno@email.com', age=30}

Process finished with exit code 0
```

