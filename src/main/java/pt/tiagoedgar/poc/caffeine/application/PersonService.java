package pt.tiagoedgar.poc.caffeine.application;

import pt.tiagoedgar.poc.caffeine.model.Person;

import java.util.List;

public interface PersonService {
    Person readByName(String name);
    List<Person> readAll();
}
